# Home Assistant Helm

.. warning::

    This chart has been moved to https://gitlab.com/GeorgeRaven/raven-helm-charts

A helm chart for [Home Assistant](https://www.home-assistant.io/), providing open-source home automation!

This helm chart provides easy deployment of home assistant on Kubernetes clusters.
This uses the mainline home assistant docker image, in a customisable but opinionated way.

## Quick Start

```bash
helm repo add home-assistant https://gitlab.com/api/v4/projects/55024277/packages/helm/stable
helm repo update ha
helm install ha home-assistant/ha
```

## Architecture

```mermaid

```
